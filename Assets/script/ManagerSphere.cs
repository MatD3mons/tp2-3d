﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class ManagerSphere : MonoBehaviour
{
    [SerializeField] private Vector3 position;
    [SerializeField] private GameObject prefable;
    [SerializeField] private float rayon = 2f;
    [SerializeField] private int nombre = 5;
    [SerializeField] private int Duree_du_vie = 5;

    private List<GameObject> listOfGameObject = new List<GameObject>();
    private int spawnnombre;
    private float spawnrayon;
    private Quaternion spawnR;
    private Vector3 spawnP;
    private float timer = 0f;

    // Start is called before the first frame update
    void Start()
    {
    }

    //Fonction qui crée un cercle de Lapin qui regarde au centre
    public List<GameObject> createCircle()
    {
        List<GameObject> list = new List<GameObject>();
        Vector3 angle = new Vector3(0,2 * Mathf.PI / nombre,0);
        for (int i = 0; i < nombre; i++)
        {
            float newX = position.x + Mathf.Cos(angle.y * i - Mathf.PI) * rayon;
            float newY = position.y;
            float newZ = position.z + Mathf.Sin(angle.y * i - Mathf.PI) * rayon;
            Vector3 newP = new Vector3(newX,newY,newZ);
            GameObject go = Instantiate(prefable,
                newP,   
                //Le lapin a était construit avec un angle de -90 degrès sur l'axe de Z :
                Quaternion.FromToRotation(new Vector3(0,0,1),position-newP),
                transform);
            go.GetComponent<Animator>().SetTrigger("Move");
            list.Add(go);
        }
        return list;
    }

    //Fonction qui destruit les lapin dans une liste
    public void kill(List<GameObject> listOfGameObject)
    {
        foreach (GameObject go in listOfGameObject)
            if (go != null)
                Destroy(go);
    }

    // Update is called once per frame
    void Update()
    {
        //On check si un changement a eu lieux.
        if(nombre != spawnnombre || rayon != spawnrayon || position != spawnP || transform.rotation != spawnR)
        {
            //On destruit tout les ancien lapin
            kill(listOfGameObject);
            //On check si il y a au moin 1 lapin
            if (nombre == 0) { return; }
            //On crée tout les lapin
            listOfGameObject = createCircle();
            foreach (GameObject go in listOfGameObject)
            {
                go.GetComponent<moveRabbit>().enabled = false;
                go.GetComponent<Animator>().SetTrigger("Idle");
            }
            spawnnombre = nombre;
            spawnrayon = rayon;
            spawnP = position;
            spawnR = transform.rotation;
        }
        timer += Time.deltaTime;
        if (timer > 1f)
        {
            timer = 0f;
            if (nombre == 0) { return; }
            float angle = (float)(2 * Mathf.PI / nombre);
            List<GameObject> list = createCircle();
            //Après 3 seconde de delay, les lapin meurt, sniff sniff
            Task.Factory.StartNew(() => Thread.Sleep(Duree_du_vie * 1000)).ContinueWith((t) => {
                foreach (GameObject go in list)
                    if (go != null)
                    {
                        go.GetComponent<Animator>().SetTrigger("Death");
                        go.GetComponent<moveRabbit>().enabled = false;
                    }
            }, TaskScheduler.FromCurrentSynchronizationContext());
            //Après 4 seconde de delay, on destruit la liste de lapin
            Task.Factory.StartNew(() => Thread.Sleep((Duree_du_vie+1) * 1000)).ContinueWith((t1) => {
                kill(list);
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
