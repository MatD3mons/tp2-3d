﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveRabbit : MonoBehaviour
{
    private Vector3 V;
    private float vitesse = -0.01f;
    private Vector3 vites;

    // Awake ( tout premier appelation )
    private void Awake()
    {
        V = transform.position;
        vites.x = Random.Range(-0.04f, 0.04f);
        vites.y = Random.Range(-0.04f, 0.04f);
        vites.z = Random.Range(-0.04f, 0.04f);
    }

    // Start is called before the first frame update ( 1ère frame )
    void Start()
    {
    }

    // Update is called once per frame ( n frame )
    void Update()
    {
        transform.position = new Vector3(transform.position.x + vites.x, transform.position.y + vitesse, transform.position.z + vites.z);
        transform.rotation = Quaternion.FromToRotation(new Vector3(0, 0, 1), new Vector3(vites.x, 0, vites.z));
        if (vitesse > 0)
        {
            if (vitesse > 0.001)
                vitesse = vitesse - 0.002f;
            else
                vitesse = -vitesse;
        }
        else
        {
            if (vitesse > -1)
                vitesse = vitesse - 0.001f;
        }

        if (transform.position.y > V.y)
            vitesse = -vitesse;
        else if (transform.position.y < 0)
            vitesse = -vitesse;
    }

    // Update après tout les Update
    private void LateUpdate()
    {

    }

}
